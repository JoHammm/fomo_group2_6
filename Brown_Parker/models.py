from django.db import models
from django.contrib.auth.models import AbstractUser

class Question(models.Model):
    question_text = models.TextField()
    pub_date = models.DateTimeField('date published')
    category = models.TextField(null=True)

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
# Define models here

class Fomo_User(AbstractUser):
    address = models.TextField()
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=100)
    zip = models.IntegerField(default=0)
    phone = models.IntegerField(default=0)
    
    #username
    #first_name
    #last_name
    #password
    #email
    #The items above are already a part of the AbstractUser class (inheritance)

    
    #code review
    #All of this below needs to be on bitbucket
#1 - models.py with User object
#2 - initialize.py 
    #create 4-5 Users
    #select with .get, .filter, .exclude and print some attributes out
    