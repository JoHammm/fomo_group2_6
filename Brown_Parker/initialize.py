import os
from datetime import datetime

#initialize django environment
#this file is to test data. put pseudo data in while developing
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()

from account.models import Question, Choice, Fomo_User

q1 = Question()
q1.question_text = 'Is it snowing today?'
q1.pub_date = datetime(2017, 4, 1, 5, 15)
q1.category = 'Day Stuff'
q1.save()

c1 = Choice()
c1.question = q1
c1.choice_text = 'Yes'
c1.save()

c2 = Choice()
c2.question = q1
c2.choice_text = 'No'
c2.save()

u1 = Fomo_User()
u1.username = 'janetmiller1'
u1.first_name = 'Janet'
u1.last_name = 'Miller'
u1.password = '11Jmiller'
u1.email = 'janetmillermusic@gmail.com'

u2 = Fomo_User()
u2.username = 'janetmiller1'
u2.first_name = 'Janet'
u2.last_name = 'Miller'
u2.password = '11Jmiller'
u2.email = 'janetmillermusic@gmail.com'

u3 = Fomo_User()
u3.username = 'janetmiller1'
u3.first_name = 'Janet'
u3.last_name = 'Miller'
u3.password = '11Jmiller'
u3.email = 'janetmillermusic@gmail.com'

u4 = Fomo_User()
u4.username = 'janetmiller1'
u4.first_name = 'Janet'
u4.last_name = 'Miller'
u4.password = '11Jmiller'
u4.email = 'janetmillermusic@gmail.com'