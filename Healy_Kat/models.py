from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class Question(models.Model):
	quesiton_text = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published')
	category = models.TextField(null=True)

class Choice(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	choice_text = models.CharField(max_length=200)
	votes = models.IntegerField(default=0)

class Fomo_User(AbstractUser):
	address = models.TextField()
	city = models.CharField(max_length=200)
	state = models.CharField(max_length=200)
	zip = models.IntegerField(default=0)
	phone = models.IntegerField(default=0)