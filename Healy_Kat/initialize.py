import os
from datetime import datetime

#initialize django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()

from account.models import Question, Choice, Fomo_User

#1 - models.py (User)
#2 - initialize.py with 4 - 5 user objects, make sure this goes in BitBucket repo
#		select (.get, .filter, .exclude) and print some attributes
