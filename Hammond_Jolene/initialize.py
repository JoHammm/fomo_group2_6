from datetime import datetime
import os

#initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()

from account.models import FomoUser

#u = FomoUser()
#u.first_name = 'Janet'
#u.last_name = 'Miller'
#u.email = 'janet.miller@gmail.com'
#u.is_staff = 0
#u.is_active = 1
#u.date_joined = datetime(2017, 6, 1, 5, 0)
#u.last_login = datetime(2017, 6, 1, 5, 0)

#u.username = 'janet'
#u.set_password('janet1')
#u.address = '123 East Street'
#u.city = 'Baltimore'
#u.state = 'Maryland'
#u.zipcode = '21117'
#u.phone = '(123) 123-1234'
#u.save()

#u2 = FomoUser()
#u2.first_name = 'Curtis'
#u2.last_name = 'Fujimoto'
#u2.email = 'curtis.fujimoto@gmail.com'
#u2.date_joined = datetime(2017, 6, 1, 5, 0)
#u2.last_login = datetime(2017, 6, 1, 5, 0)
#u2.username = 'curtis'
#u2.set_password('curtis1')
#u2.address = '123 East Street'
#u2.city = 'Lehi'
#u2.state = 'Utah'
#u2.zipcode = '84043'
#u2.phone = '(123) 123-1234'
#u2.save()

#u3 = FomoUser()
#u3.first_name = 'Katelyn'
#u3.last_name = 'Ward'
#u3.email = 'katelyn.ward@gmail.com'
#u3.date_joined = datetime(2017, 6, 1, 5, 0)
#u3.last_login = datetime(2017, 6, 1, 5, 0)
#u3.username = 'katelyn'
#u3.set_password('katelyn1')
#u3.address = '123 East Street'
#u3.city = 'Salt Lake City'
#u3.state = 'Utah'
#u3.zipcode = '84101'
#u3.phone = '(123) 123-1234'
#u3.save()

#u4 = FomoUser()
#u4.first_name = 'Frank'
#u4.last_name = 'Jacobs'
#u4.email = 'frank.jacobs@gmail.com'
#u4.date_joined = datetime(2017, 6, 1, 5, 0)
#u4.last_login = datetime(2017, 6, 1, 5, 0)
#u4.username = 'frank'
#u4.set_password('frank1')
#u4.address = '123 East Street'
#u4.city = 'Baltimore'
#u4.state = 'Maryland'
#u4.zipcode = '21117'
#u4.phone = '(123) 123-1234'
#u4.save()

u1 = FomoUser.objects.get(id=2)
print(u1.first_name)

u2 = FomoUser.objects.filter(id__gt=3)
for u in u2:
    print(u.id)
