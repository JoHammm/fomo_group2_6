from django.db import models
from django.contrib.auth.models import AbstractUser

# Account Models.
class FomoUser(AbstractUser):
    #inheriting from super
    #id
    #first_name
    #last_name
    #username
    #email
    #is_staff
    #is_active
    #date_joined
    #objects
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=60)
    state = models.CharField(max_length=60)
    zipcode = models.CharField(max_length=20)
    phone = models.CharField(max_length=30)
