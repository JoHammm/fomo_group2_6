from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from django.http import HttpResponseRedirect
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    productID = request.urlparams[0]
    try:
        product = cmod.Product.objects.get(id=productID)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/products/')

    # Make current product isn't in list
    while product.id in request.last5:
         request.last5.remove(product.id)
    #request.last5.insert(0, product.id)

    # Actually adding the item to request
    request.last5.insert(0, product.id)
    #making a copy of the list to splice so that I do not delete any extra items
    last5copy = request.last5
    if len(last5copy) > 5:
        last5copy = last5copy[1:6]

    last5products = list(map(lambda item: cmod.Product.objects.get(id=item), last5copy))

    return dmp_render(request, 'details.html', {
        'product': product,
        'last5products': last5products,
        })
