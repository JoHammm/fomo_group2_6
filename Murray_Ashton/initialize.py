from django.core import management
from django.db import connection
from datetime import datetime
import os, os.path, sys

# ensure the user really wants to do this
areyousure = input('''
  You are about to drop and recreate the entire database.
  All data are about to be deleted.  Use of this script
  may cause itching, vertigo, dizziness, tingling in
  extremities, loss of balance or coordination, slurred
  speech, temporary zoobie syndrome, longer lines at the
  testing center, changed passwords in Learning Suite, or
  uncertainty about whether to call your professor
  'Brother' or 'Doctor'.

  Please type 'yes' to confirm the data destruction: ''')
if areyousure.lower() != 'yes':
    print()
    print('  Wise choice.')
    sys.exit(1)

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# drop and recreate the database tables
print()
print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')


# imports for our project
from account.models import FomoUser


u = FomoUser()
u.first_name = 'Janet'
u.last_name = 'Miller'
u.email = 'janet.miller@gmail.com'
u.is_staff = 0
u.is_active = 1
u.date_joined = datetime(2017, 4, 1, 5, 0, 0)
u.last_login = datetime(2017, 4, 1, 5, 0)
u.username = 'janet'
u.set_password('janetspassword')
u.address = '123 seseme Street'
u.city = 'Baltimore'
u.state = 'Maryland'
u.zipcode = '21117'
u.phone = '(123) 456-7891'
u.save()

u2 = FomoUser()
u2.first_name = 'Curtis'
u2.last_name = 'Fujimoto'
u2.email = 'curtis.fujimoto@gmail.com'
u2.date_joined = datetime(2017, 3, 1, 5, 0)
u2.last_login = datetime(2017, 3, 1, 5, 0)
u2.username = 'curtis'
u2.set_password('curtisspassword')
u2.address = '123 Seseme Street'
u2.city = 'Lehi'
u2.state = 'Utah'
u2.zipcode = '84043'
u2.phone = '(123) 456-7891'
u2.save()

u3 = FomoUser()
u3.first_name = 'Katelyn'
u3.last_name = 'Ward'
u3.email = 'katelyn.ward@gmail.com'
u3.date_joined = datetime(2017, 2, 1, 5, 0)
u3.last_login = datetime(2017, 2, 1, 5, 0)
u3.username = 'katelyn'
u3.set_password('katelynspasword')
u3.address = '123 Seseme Street'
u3.city = 'Salt Lake City'
u3.state = 'Utah'
u3.zipcode = '84101'
u3.phone = '(123) 456-7891'
u3.save()

u4 = FomoUser()
u4.first_name = 'Frank'
u4.last_name = 'Jacobs'
u4.email = 'frank.jacobs@gmail.com'
u4.date_joined = datetime(2017, 1, 1, 5, 0)
u4.last_login = datetime(2017, 1, 1, 5, 0)
u4.username = 'frank'
u4.set_password('franks password')
u4.address = '123 Seseme Street'
u4.city = 'Baltimore'
u4.state = 'Maryland'
u4.zipcode = '21117'
u4.phone = '(123) 456-7891'
u4.save()

u1 = FomoUser.objects.get(id=4)
print('Look up user by ID: ', u1.first_name)

u2 = FomoUser.objects.filter(id__gt=1)
for u in u2:
    print('ID: ', u.id)
    print('First Name:', u.first_name)
