def Last5ProductsMiddleware(get_response):

    def middleware(request):
        #Code to be executed for each request before the view and later missleare are called

        request.last5 = request.session.get('last5')
        if request.last5 is None:
            request.last5 = []

        response = get_response(request)
        # Code to be executed for each request/response after the view is called.

        # Slices off the last fifteen because I want extra just in case and makes the session's list 5 equal to request.last5
        request.session['last5'] = request.last5[:15]
        return response
    return middleware
